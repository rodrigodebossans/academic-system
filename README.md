# Academic System

## Objetivo

Mostrar o andamento das avaliações dos alunos em cada de tipo de avaliação (AV1, AV2, AV3) e permitir o lançamento de notas dos mesmos.

### Requerimentos

- Tomcat >= 9.0.39
- Java >= 8
- MySQL Community Server >= 8.0.22
- Maven >= 3.6.3

### Uso

#### Levantando a aplicação

1 - Clone o repositório

```sh
  git clone https://rodrigodebossans@bitbucket.org/rodrigodebossans/academic-system.git
```

2 - Crie um arquivo chamado `application.properties` dentro de `src/main/recources` seguindo o modelo disponibilizado no arquivo `application.properties.example` e informe suas credenciais de acesso à base de dados

3 - Na pasta do projeto utilize o maven para fazer o download das dependências e gere o arquivo para deploy (academic-sistem.war)

```sh
  mvn install
```

```sh
  mvn war:war -f pom.xml
```

4 - Por fim mova o arquivo `academic-sistem.war` para a pasta webapps do Tomcat

```sh
  cp target/academic-system.war /path/to/tomcat/webapps
```

### Sobre

#### O sistema foi construído considerando o minimundo abaixo

Minimundo: Em uma universidade, as avaliações de uma disciplina são formadas por três notas: AV1, AV2 e AV3. A nota AV1 é composta por um trabalho acadêmico, que vale 7,0 pontos, e uma APS que vale 3,0 pontos. A AV2 é composta por um trabalho acadêmico, que vale 8,0 pontos, e uma APS que vale 2,0 pontos. A AV3 é composta apenas por um trabalho acadêmico que vale 10,0 pontos. O aluno é considerado aprovado se a média de duas avaliações quaisquer é maior ou igual a 7,0. Cada aluno pertencerá a um curso de graduação que poderá fazer de 1 (uma) a 10 (dez) disciplinas, sendo que cada disciplina será regida pelas regras das avaliações acima descritas.

Para este obejtivo, foi desenvolvido o seguinte diagrama ER

![picture](https://i.ibb.co/c1VRHk0/diagrama-ER.png)

### Acessando a aplicação

#### Login como professor

`Usuário: 2018200084`
`Senha: 123456789`
`Perfil: Professor`

#### Login como aluno

`Usuário: 2018200085`
`Senha: 123456789`
`Perfil: Aluno`

#### Enjoy
