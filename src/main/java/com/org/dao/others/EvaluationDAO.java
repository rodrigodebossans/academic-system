package com.org.dao.others;

import com.org.models.others.Evaluation;
import java.util.List;
import com.org.utils.hibernate.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class EvaluationDAO {
  private static EvaluationDAO instance;
  
  public EvaluationDAO() {
  }

  public synchronized static EvaluationDAO getInstance() {
    if (instance == null) {
      instance = new EvaluationDAO();
    }
    return instance;
  }
  
  public void updateFinalNote(Integer id, Float note) {
    try {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      Evaluation evaluation = (Evaluation) session.get(Evaluation.class, id);
      evaluation.setFinalNote(note);
      session.save(evaluation);

      session.getTransaction().commit();
      session.close();
    } catch (HibernateException e) {
      e.printStackTrace();
    }
  }
  
  public List<Evaluation> getEvaluationListByRegistration(String registration) {
    try {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
        
      Query userHQL = session.createQuery("FROM Evaluation e WHERE e.studentClass.student.user.registration = :registration");
      userHQL.setParameter("registration", registration);
      List<Evaluation> evaluationList = (List<Evaluation>) userHQL.list();
        
      session.getTransaction().commit();
      session.close();
        
      return evaluationList;
    } catch (HibernateException e) {
      e.printStackTrace();
    }

    return null;
  }
}