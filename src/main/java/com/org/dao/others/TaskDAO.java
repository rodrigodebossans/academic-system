package com.org.dao.others;

import com.org.models.others.Task;
import java.util.List;
import com.org.utils.hibernate.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class TaskDAO {
  private static TaskDAO instance;
  
  public TaskDAO() {
  }

  public synchronized static TaskDAO getInstance() {
    if (instance == null) {
      instance = new TaskDAO();
    }
    return instance;
  }
  
  public List<Task> getTaskListByEvaluationId(Integer evaluationId) {
    try {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
        
      Query queryHQL = session.createQuery("FROM Task t WHERE t.evaluation.id = : evaluationId");
      queryHQL.setParameter("evaluationId", evaluationId);
      List<Task> taskList = (List<Task>) queryHQL.list();
        
      session.getTransaction().commit();
      session.close();
        
      return taskList;
    } catch (HibernateException e) {
        e.printStackTrace();
    }

    return null;
  }
}