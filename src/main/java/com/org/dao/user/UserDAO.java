package com.org.dao.user;

import java.util.List;
import com.org.models.user.Student;
import com.org.utils.hibernate.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public class UserDAO {
  private static UserDAO instance;
  
  public UserDAO() {
  }

  public synchronized static UserDAO getInstance() {
    if (instance == null) {
      instance = new UserDAO();
    }
    return instance;
  }
  
  public List<Student> getUserStudentsList() {
    try {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();
        
      List<Student> studentList = (List<Student>) session.createQuery("FROM Student s").list();

      session.getTransaction().commit();
      session.close();
        
      return studentList;
    } catch (HibernateException e) {
        e.printStackTrace();
    }

    return null;
  }
}