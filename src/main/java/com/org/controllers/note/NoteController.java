/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.org.controllers.note;

import com.org.dao.others.EvaluationDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import com.org.models.others.Evaluation;
import com.org.services.user.StudentService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;

@WebServlet("/noteController")
public class NoteController extends HttpServlet {

  protected void processRequestGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    try (PrintWriter out = response.getWriter()) {
      StudentService studentService = new StudentService();
      String registration = request.getParameter("registration");

      List<Evaluation> evaluationList = studentService.getEvaluationsByRegistration(registration);

      List<Object> evaluationTypeList = new ArrayList<Object>();

      evaluationList.forEach(evaluation -> {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("evaluationId", evaluation.getId());
        properties.put("evaluationType", evaluation.getEvaluationType().getName());
        evaluationTypeList.add(properties);
      });

      ObjectMapper Obj = new ObjectMapper();
      String jsonStr = Obj.writeValueAsString(evaluationTypeList);
      out.println(jsonStr);
    }
  }


  protected void processRequestPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    Integer evaluationTypeId = Integer.parseInt(request.getParameter("evaluationType"));
    String noteStr = request.getParameter("note");

    if (noteStr.contains(","))
      noteStr = noteStr.replace(",", ".");

    Float note = Float.parseFloat(noteStr);
    EvaluationDAO evaluationDAO = EvaluationDAO.getInstance();
    evaluationDAO.updateFinalNote(evaluationTypeId, note);

    response.sendRedirect(request.getHeader("referer"));
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequestPost(request, response);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequestGet(request, response);
  }

  @Override
  public String getServletInfo() {
    return "Short description";
  }
}
