package com.org.controllers.auth;

import com.org.services.bootstrap.DatabaseBootstrapService;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.org.models.user.User;
import com.org.services.auth.AuthService;

@WebServlet("/loginController")
public class LoginController extends HttpServlet {

  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    AuthService authService = new AuthService();
    DatabaseBootstrapService databaseBootstrapService = new DatabaseBootstrapService();

    String registration = request.getParameter("registration");
    String password = request.getParameter("password");

    try {
      if (databaseBootstrapService.databaseDoesNotExist()) {
        databaseBootstrapService.createDatabase();

        if (databaseBootstrapService.usersDoNotExist()) {
          databaseBootstrapService.createUsers();
          databaseBootstrapService.insertRequiredData();
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    User userLogged = authService.authenticate(registration, password);

    if(userLogged != null) {
      request.getSession().setAttribute("userLogged", userLogged);
      request.getSession().removeAttribute("authErrorMessage");
      response.sendRedirect("home.jsp");
    } else {
      request.getSession().setAttribute("authErrorMessage", "invalidUser");
      response.sendRedirect(request.getHeader("referer"));
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }
}
