package com.org.services.user;

import com.org.dao.user.UserDAO;
import com.org.models.user.User;
import com.org.utils.hibernate.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import org.hibernate.Session;
import org.hibernate.query.Query;


public class UserService {
  UserDAO userDAO = UserDAO.getInstance();
  Session session;

  public UserService() {
  }

  public void deleteUser(String registration) {
    this.deleteByRegistration(registration);
  }

  public boolean isStudent(String registration) {
    User user = this.findByRegistration(registration);

    boolean userIsStudent = user.getRole().getName().equalsIgnoreCase("aluno");

    if (user != null && userIsStudent)
      return true;

    return false;
  }

  public boolean isTeacher(String registration) {
    User user = this.findByRegistration(registration);

    boolean userIsTeacher = user.getRole().getName().equalsIgnoreCase("professor");

    return user != null && userIsTeacher;
  }

  public User findByRegistration(String registration) {
    try {
      this.session = HibernateUtil.getSessionFactory().openSession();
      this.session.beginTransaction();

      Query userHQL = this.session.createQuery("FROM User u WHERE u.registration = :registration");
      userHQL.setParameter("registration", registration);
      List<User> userList = (List<User>) userHQL.list();

      Predicate<User> userPredicate = user-> user.getRegistration().equals(registration);
      User user = userList.stream().filter(userPredicate).findFirst().orElse(null);

      this.session.getTransaction().commit();
      this.session.close();

      if (user != null)
        return user;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public List<User> getStudentList() {
    List<User> userList = new ArrayList<User>();

    this.userDAO.getUserStudentsList().forEach(student -> {
      userList.add(student.getUser());
    });

    return userList;
  }

  public void deleteByRegistration(String registration) {
    User userToDelete = this.findByRegistration(registration);
    if (userToDelete != null) {
      Predicate<User> deleteUserPredicate = user -> Objects.equals(user.getRegistration(), registration);
    }
  }
}
