package com.org.services.user;

import com.org.dao.others.EvaluationDAO;
import com.org.models.others.Evaluation;
import com.org.models.others.StudentClass;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class StudentService {
  EvaluationDAO evaluationDAO = EvaluationDAO.getInstance();

  public StudentService() {
  }

  public List<Evaluation> getEvaluationsByRegistration(String registration) {
    return evaluationDAO.getEvaluationListByRegistration(registration);
  }


  public Float getAverageByRating(List<Evaluation> evaluationList) {
    if (evaluationList.size() >= 2) {

      Float lowestFinalNote = Float.parseFloat("11");

      for (Evaluation evaluation : evaluationList) {
        if (evaluation.getFinalNote() < lowestFinalNote) {
          lowestFinalNote = evaluation.getFinalNote();
        }
      }

      final Float lFinalNote = lowestFinalNote;

      Predicate<Evaluation> removeLowestNotePredicate = user -> !user.getFinalNote().equals(lFinalNote);
      List<Evaluation> notesToConsider = evaluationList.stream().filter(removeLowestNotePredicate).collect(Collectors.toList());

      if (notesToConsider.size() == 2) {
        return (notesToConsider.get(0).getFinalNote() + notesToConsider.get(1).getFinalNote()) / notesToConsider.size();
      }
    }

    return Float.parseFloat("0.0");
  }

  public String getStudentSituation(Float average) {
    if (average == 0)
      return "aguardando lançamento de nota";

    final Float minimumNoteRequiredToApprove = Float.parseFloat("7.0");
    return average > minimumNoteRequiredToApprove ? "aprovado" : "reprovado";
  }

  public StudentClass getStudentClassByRegistration(String registration) {
    return null;
  }
} 
