package com.org.services.others;

import com.org.dao.others.TaskDAO;
import com.org.models.others.Task;
import java.util.List;

public class TaskService {
    TaskDAO taskDAO = TaskDAO.getInstance();

    public TaskService() {
    }
    
    public List<Task> getTaskListByEvaluationId(Integer evaluationId) {
        return taskDAO.getTaskListByEvaluationId(evaluationId);
    }
}
