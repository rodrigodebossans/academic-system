package com.org.services.bootstrap;

import com.org.models.others.Class;
import com.org.models.others.AcademicDegree;
import com.org.models.others.Course;
import com.org.models.others.Discipline;
import com.org.models.others.Evaluation;
import com.org.models.others.EvaluationType;
import com.org.models.others.StudentClass;
import com.org.models.others.Task;
import com.org.models.others.TaskType;
import com.org.models.role.Role;
import com.org.models.user.Student;
import com.org.models.user.Teacher;
import com.org.models.user.User;
import com.org.providers.database.MySQLProvider;
import com.org.utils.hibernate.HibernateUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import org.hibernate.Session;

public class DatabaseBootstrapService {
  private Connection connection;

  public DatabaseBootstrapService() {
    try {
      this.connection = MySQLProvider.getInstance();
    } catch (ClassNotFoundException | SQLException e) {
      e.printStackTrace();
    }
  }

  public boolean usersDoNotExist() throws SQLException {
    Statement statement = this.connection.createStatement();

    String query = "SELECT host, user FROM mysql.user WHERE user LIKE 'dba' or user like 'dev'";
    statement.executeQuery(query);

    if (statement.execute(query)) {
      if (statement.getResultSet().next())
        return false;
    }

    return true;
  }

  public boolean databaseDoesNotExist() throws SQLException {
    Statement statement = this.connection.createStatement();

    String query = "SHOW DATABASES LIKE 'universidade'";
    statement.executeQuery(query);

    if (statement.execute(query)) {
      if (statement.getResultSet().next())
        return false;
    }

    return true;
  }

  public void createDatabase() throws SQLException {
    Statement statement = this.connection.createStatement();

    statement.executeUpdate("CREATE SCHEMA IF NOT EXISTS `universidade` DEFAULT CHARACTER SET utf8");

    statement.executeUpdate("USE `universidade`");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_GRAU_ACADEMICO` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`nome` VARCHAR(100) NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC)," +
            "UNIQUE INDEX `nome_UNIQUE` (`nome` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_CURSO` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`id_grau_academico` INT UNSIGNED NOT NULL," +
            "`nome` VARCHAR(100) NOT NULL," +
            "`carga_horaria` INT NOT NULL," +
            "`cr_minimo` FLOAT NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC)," +
            "UNIQUE INDEX `nome_UNIQUE` (`nome` ASC)," +
            "FOREIGN KEY (`id_grau_academico`)" +
            "REFERENCES `universidade`.`TB_GRAU_ACADEMICO` (`id`)" +
            ") ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_PAPEL` (" +
            "`id` INT NOT NULL AUTO_INCREMENT," +
            "`nome` VARCHAR(100) NOT NULL," +
            "PRIMARY KEY (`id`))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_USUARIO` (" +
            "`cpf` VARCHAR(100) NOT NULL," +
            "`id_role` INT NOT NULL," +
            "`matricula` VARCHAR(100) NOT NULL," +
            "`nome` VARCHAR(100) NOT NULL," +
            "`sobrenome` VARCHAR(100) NULL," +
            "`senha` VARCHAR(100) NULL," +
            "PRIMARY KEY (`cpf`)," +
            "FOREIGN KEY (`id_role`)" +
            "REFERENCES `universidade`.`TB_PAPEL` (`id`)," +
            "UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC)," +
            "UNIQUE INDEX `matricula_UNIQUE` (`matricula` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_ALUNO` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`cpf_pessoa` VARCHAR(100) NOT NULL," +
            "`id_curso` INT UNSIGNED NULL," +
            "PRIMARY KEY (`id`)," +
            "FOREIGN KEY (`id_curso`)" +
            "REFERENCES `universidade`.`TB_CURSO` (`id`)," +
            "FOREIGN KEY (`cpf_pessoa`)" +
            "REFERENCES `universidade`.`TB_USUARIO` (`cpf`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_PROFESSOR` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`cpf_pessoa` VARCHAR(100) NOT NULL," +
            "`titulacao` VARCHAR(50) NULL," +
            "PRIMARY KEY (`id`)," +
            "FOREIGN KEY (`cpf_pessoa`)" +
            "REFERENCES `universidade`.`TB_USUARIO` (`cpf`))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_DISCIPLINA` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`nome` VARCHAR(100) NOT NULL," +
            "`carga_horaria` INT NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC)," +
            "UNIQUE INDEX `nome_UNIQUE` (`nome` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_TURMA` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`id_disciplina` INT UNSIGNED NOT NULL," +
            "`id_professor` INT UNSIGNED NOT NULL," +
            "`referencia` VARCHAR(50) NOT NULL," +
            "`qt_vagas` INT UNSIGNED NOT NULL," +
            "`data_inicio` DATE NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "FOREIGN KEY (`id_disciplina`)" +
            "REFERENCES `universidade`.`TB_DISCIPLINA` (`id`)," +
            "FOREIGN KEY (`id_professor`)" +
            "REFERENCES `universidade`.`TB_PROFESSOR` (`id`))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_ALUNO_TURMA` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`id_aluno` INT UNSIGNED NOT NULL," +
            "`id_turma` INT UNSIGNED NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "FOREIGN KEY (`id_aluno`)" +
            "REFERENCES `universidade`.`TB_ALUNO` (`id`)," +
            "FOREIGN KEY (`id_turma`)" +
            "REFERENCES `universidade`.`TB_TURMA` (`id`))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_TIPO_AVALIACAO` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`nome` VARCHAR(100) NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC)," +
            "UNIQUE INDEX `nome_UNIQUE` (`nome` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_AVALIACAO` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`id_tipo_avaliacao` INT UNSIGNED NOT NULL," +
            "`id_aluno_turma` INT UNSIGNED NOT NULL," +
            "`data_inicio` DATETIME NOT NULL," +
            "`data_fim` DATETIME NOT NULL," +
            "`nota_final` FLOAT UNSIGNED NULL DEFAULT 0," +
            "`nota_maxima` FLOAT NULL," +
            "`concluida` TINYINT(1) NOT NULL DEFAULT 0," +
            "PRIMARY KEY (`id`)," +
            "FOREIGN KEY (`id_tipo_avaliacao`)" +
            "REFERENCES `universidade`.`TB_TIPO_AVALIACAO` (`id`)," +
            "FOREIGN KEY (`id_aluno_turma`)" +
            "REFERENCES `universidade`.`TB_ALUNO_TURMA` (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_TIPO_TAREFA` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`nome` VARCHAR(100) NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC)," +
            "UNIQUE INDEX `nome_UNIQUE` (`nome` ASC))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_TAREFA` (" +
            "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT," +
            "`id_tipo_tarefa` INT UNSIGNED NOT NULL," +
            "`id_avaliacao` INT UNSIGNED NOT NULL," +
            "`descricao` LONGTEXT NULL," +
            "`nota` FLOAT NOT NULL DEFAULT 0," +
            "`concluida` TINYINT(1) NOT NULL DEFAULT 0," +
            "`nota_maxima` FLOAT UNSIGNED NOT NULL," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE INDEX `id_UNIQUE` (`id` ASC)," +
            "INDEX `fk_TB_TAREFA_TB_TIPO_TAREFA1_idx` (`id_tipo_tarefa` ASC)," +
            "INDEX `fk_TB_TAREFA_TB_AVALIACAO1_idx` (`id_avaliacao` ASC)," +
            "FOREIGN KEY (`id_tipo_tarefa`)" +
            "REFERENCES `universidade`.`TB_TIPO_TAREFA` (`id`)," +
            "FOREIGN KEY (`id_avaliacao`)" +
            "REFERENCES `universidade`.`TB_AVALIACAO` (`id`))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");

    statement.executeUpdate("CREATE TABLE IF NOT EXISTS `universidade`.`TB_CURSO_DISCIPLINA` (" +
            "`id_curso` INT UNSIGNED NOT NULL," +
            "`id_disciplina` INT UNSIGNED NOT NULL," +
            "FOREIGN KEY (`id_curso`)" +
            "REFERENCES `universidade`.`TB_CURSO` (`id`)," +
            "FOREIGN KEY (`id_disciplina`)" +
            "REFERENCES `universidade`.`TB_DISCIPLINA` (`id`))" +
            "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8");
  }

  public void createUsers() throws SQLException {
    Statement statement = this.connection.createStatement();

    statement.execute("CREATE USER 'dba'@'localhost'");
    statement.execute("ALTER USER 'dba'@'localhost' IDENTIFIED BY 'Universidade123,'");
    statement.execute("GRANT ALL PRIVILEGES ON universidade.* TO 'dba'@'localhost'");
    statement.execute("CREATE USER 'dev'@'localhost'");
    statement.execute("ALTER USER 'dev'@'localhost' IDENTIFIED BY 'Universidade123,'");
    statement.execute("GRANT Alter ON universidade.* TO 'dev'@'localhost'");
    statement.execute("GRANT Create ON universidade.* TO 'dev'@'localhost'");
    statement.execute("GRANT Delete ON universidade.* TO 'dev'@'localhost'");
    statement.execute("GRANT Select ON universidade.* TO 'dev'@'localhost'");
    statement.execute("GRANT Show view ON universidade.* TO 'dev'@'localhost'");
    statement.execute("GRANT Update ON universidade.* TO 'dev'@'localhost'");
    statement.execute("GRANT Insert ON universidade.* TO 'dev'@'localhost'");
  }

  public void insertRequiredData() {
    try {
      Session session = HibernateUtil.getSessionFactory().openSession();
      session.beginTransaction();

      // insere um grau acadêmico
      AcademicDegree academicDegree = new AcademicDegree();
      academicDegree.setName("Graduação");
      session.save(academicDegree);

      // insere uma disciplina
      Discipline discipline = new Discipline();
      discipline.setName("Algoritmos 1");
      discipline.setWorkload(60);
      session.save(discipline);

      // insere um curso de graduação e relaciona a disciplina de Algoritmos 1
      Course course = new Course();
      course.setName("Ciência da computação");
      course.setWorkload(3000);
      course.setCr_minimo(Float.parseFloat("7.0"));
      course.setAcademicDegree(academicDegree);
      course.addDiscipline(discipline);
      session.save(course);

      // insere uma função de aluno
      Role roleStudent = new Role();
      roleStudent.setName("Aluno");
      session.save(roleStudent);

      // insere uma função de professor
      Role roleTeacher = new Role();
      roleTeacher.setName("Professor");
      session.save(roleTeacher);

      // insere um usuário com a função de professor
      User userTeacher = new User();
      userTeacher.setCpf("17412208748");
      userTeacher.setRole(roleTeacher);
      userTeacher.setRegistration("2018200084");
      userTeacher.setName("Júlio");
      userTeacher.setSurname("Tadeu");
      userTeacher.setPassword("123456789");
      session.save(userTeacher);

      // insere um usuário com função de aluno
      User userStudent = new User();
      userStudent.setCpf("17412208749");
      userStudent.setRole(roleStudent);
      userStudent.setRegistration("2018200085");
      userStudent.setName("Rodrigo");
      userStudent.setSurname("Debossans");
      userStudent.setPassword("123456789");
      session.save(userStudent);

      // insere outro usuário com função de aluno
      User userStudent2 = new User();
      userStudent2.setCpf("17412208750");
      userStudent2.setRole(roleStudent);
      userStudent2.setRegistration("2018200086");
      userStudent2.setName("Marco");
      userStudent2.setSurname("Bacelo");
      userStudent2.setPassword("123456789");
      session.save(userStudent2);

      // insere um professor
      Teacher teacher = new Teacher();
      teacher.setTitration("Dr");
      teacher.setUser(userTeacher);
      session.save(teacher);

      // insere um estudante no curso de ciência da computação
      Student student = new Student();
      student.setUser(userStudent);
      student.setCourse(course);
      session.save(student);

      // insere um estudante com o usuário acima no curso de ciência da computação
      Student student2 = new Student();
      student2.setUser(userStudent2);
      student2.setCourse(course);
      session.save(student2);

      // insere uma turma
      Class classe = new Class();
      classe.setDiscipline(discipline);
      classe.setInitDate(new Date());
      classe.setNumberOfVacancies(60);
      classe.setTeacher(teacher);
      classe.setReference("851");

      // matricula um aluno na turma 851
      StudentClass studentClass = new StudentClass();
      studentClass.setClasse(classe);
      studentClass.setStudent(student);

      // matricula outro aluno na turma 851
      StudentClass studentClass2 = new StudentClass();
      studentClass2.setClasse(classe);
      studentClass2.setStudent(student2);
      classe.addStudentClass(studentClass);
      classe.addStudentClass(studentClass2);
      session.save(classe);

      // insere um tipo de avaliação AV1
      EvaluationType evaluationType = new EvaluationType();
      evaluationType.setName("AV1");
      session.save(evaluationType);

      // insere um tipo de avaliação AV3
      EvaluationType evaluationType2 = new EvaluationType();
      evaluationType2.setName("AV2");
      session.save(evaluationType2);

      // insere um tipo de avaliação AV3
      EvaluationType evaluationType3 = new EvaluationType();
      evaluationType3.setName("AV3");
      session.save(evaluationType3);

      // insere uma avaliação do tipo AV1 pro usuario 1
      Evaluation evaluationAV1 = new Evaluation();
      evaluationAV1.setEvaluationType(evaluationType);
      evaluationAV1.setStudentClass(studentClass);
      evaluationAV1.setInitDate(new Date());
      evaluationAV1.setEndDate(new Date());
      evaluationAV1.setFinalNote(Float.parseFloat("0.0"));
      evaluationAV1.setMaxNote(Float.parseFloat("10.0"));
      evaluationAV1.setCompleted(true);
      session.save(evaluationAV1);

      // insere uma avaliação do tipo AV2 pro usuario 1
      Evaluation evaluationAV2 = new Evaluation();
      evaluationAV2.setEvaluationType(evaluationType2);
      evaluationAV2.setStudentClass(studentClass);
      evaluationAV2.setInitDate(new Date());
      evaluationAV2.setEndDate(new Date());
      evaluationAV2.setFinalNote(Float.parseFloat("0.0"));
      evaluationAV2.setMaxNote(Float.parseFloat("10.0"));
      evaluationAV2.setCompleted(true);
      session.save(evaluationAV2);

      // insere uma avaliação do tipo AV3 pro usuario 1
      Evaluation evaluationAV3 = new Evaluation();
      evaluationAV3.setEvaluationType(evaluationType3);
      evaluationAV3.setStudentClass(studentClass);
      evaluationAV3.setInitDate(new Date());
      evaluationAV3.setEndDate(new Date());
      evaluationAV3.setFinalNote(Float.parseFloat("0.0"));
      evaluationAV3.setMaxNote(Float.parseFloat("10.0"));
      evaluationAV3.setCompleted(true);
      session.save(evaluationAV3);

      // insere uma avaliação do tipo AV1 pro usuario 2
      Evaluation evaluation2AV1 = new Evaluation();
      evaluation2AV1.setEvaluationType(evaluationType);
      evaluation2AV1.setStudentClass(studentClass2);
      evaluation2AV1.setInitDate(new Date());
      evaluation2AV1.setEndDate(new Date());
      evaluation2AV1.setFinalNote(Float.parseFloat("0.0"));
      evaluation2AV1.setMaxNote(Float.parseFloat("10.0"));
      evaluation2AV1.setCompleted(true);
      session.save(evaluation2AV1);

      // insere uma avaliação do tipo AV2 pro usuario 2
      Evaluation evaluation2AV2 = new Evaluation();
      evaluation2AV2.setEvaluationType(evaluationType2);
      evaluation2AV2.setStudentClass(studentClass2);
      evaluation2AV2.setInitDate(new Date());
      evaluation2AV2.setEndDate(new Date());
      evaluation2AV2.setMaxNote(Float.parseFloat("10.0"));
      evaluation2AV2.setCompleted(true);
      session.save(evaluation2AV2);

      // insere uma avaliação do tipo AV3 pro usuario 2
      Evaluation evaluation2AV3 = new Evaluation();
      evaluation2AV3.setEvaluationType(evaluationType3);
      evaluation2AV3.setStudentClass(studentClass2);
      evaluation2AV3.setInitDate(new Date());
      evaluation2AV3.setEndDate(new Date());
      evaluation2AV3.setFinalNote(Float.parseFloat("0.0"));
      evaluation2AV3.setMaxNote(Float.parseFloat("10.0"));
      evaluation2AV3.setCompleted(true);
      session.save(evaluation2AV3);

      // insere um tipo de tarefa do tipo trabalho acadêmico
      TaskType taskTypeTA = new TaskType();
      taskTypeTA.setName("Trabalho acadêmico");
      session.save(taskTypeTA);

      // insere um tipo de tarefa do tipo APS
      TaskType taskTypeAPS = new TaskType();
      taskTypeAPS.setName("Atividade prática supervisionada");
      session.save(taskTypeAPS);

      // insere uma tarefa da AV1 do tipo trabalho acadêmico pro usuario 1
      Task taskTAAV1 = new Task();
      taskTAAV1.setTaskType(taskTypeTA);
      taskTAAV1.setEvaluation(evaluationAV1);
      taskTAAV1.setDescription("Este é um exemplo do que poderia ser um requisito para o trabalho acadêmico da AV1");
      taskTAAV1.setNote(Float.parseFloat("5.5"));
      taskTAAV1.setMaxNote(Float.parseFloat("7.0"));
      taskTAAV1.setCompleted(true);
      session.save(taskTAAV1);

      // insere uma tarefa da AV1 do tipo APS pro usuario 1
      Task taskAPSAV1 = new Task();
      taskAPSAV1.setTaskType(taskTypeAPS);
      taskAPSAV1.setEvaluation(evaluationAV1);
      taskAPSAV1.setDescription("Este é um exemplo do que poderia ser um requisito para APS da AV1");
      taskAPSAV1.setNote(Float.parseFloat("2"));
      taskAPSAV1.setMaxNote(Float.parseFloat("3.0"));
      taskAPSAV1.setCompleted(true);
      session.save(taskAPSAV1);

      // insere uma tarefa da AV2 do tipo trabalho acadêmico pro usuario 1
      Task taskTAAV2 = new Task();
      taskTAAV2.setTaskType(taskTypeTA);
      taskTAAV2.setEvaluation(evaluationAV2);
      taskTAAV2.setDescription("Este é um exemplo do que poderia ser um requisito para o trabalho acadêmico da AV2");
      taskTAAV2.setNote(Float.parseFloat("5.0"));
      taskTAAV2.setMaxNote(Float.parseFloat("8.0"));
      taskTAAV2.setCompleted(true);
      session.save(taskTAAV2);

      // insere uma tarefa da AV2 do tipo APS pro usuario 1
      Task taskAPSAV2 = new Task();
      taskAPSAV2.setTaskType(taskTypeAPS);
      taskAPSAV2.setEvaluation(evaluationAV2);
      taskAPSAV2.setDescription("Este é um exemplo do que poderia ser um requisito para a APS da AV2");
      taskAPSAV2.setNote(Float.parseFloat("1.0"));
      taskAPSAV2.setMaxNote(Float.parseFloat("2.0"));
      taskAPSAV2.setCompleted(true);
      session.save(taskAPSAV2);

      // insere uma tarefa da AV3 do tipo trabalho acadêmico pro usuario 1
      Task taskTAAV3 = new Task();
      taskTAAV3.setTaskType(taskTypeTA);
      taskTAAV3.setEvaluation(evaluationAV3);
      taskTAAV3.setDescription("Este é um exemplo do que poderia ser um requisito para a o trabalho acadêmico da AV3");
      taskTAAV3.setNote(Float.parseFloat("9.0"));
      taskTAAV3.setMaxNote(Float.parseFloat("10.0"));
      taskTAAV3.setCompleted(true);
      session.save(taskTAAV3);

      // insere uma tarefa da AV1 do tipo trabalho acadêmico pro usuario 2
      Task task2TAAV1 = new Task();
      task2TAAV1.setTaskType(taskTypeTA);
      task2TAAV1.setEvaluation(evaluation2AV1);
      task2TAAV1.setDescription("Este é um exemplo do que poderia ser um requisito para o trabalho acadêmico da AV1");
      task2TAAV1.setMaxNote(Float.parseFloat("7.0"));
      task2TAAV1.setCompleted(true);
      session.save(task2TAAV1);

      // insere uma tarefa da AV1 do tipo APS pro usuario 2
      Task task2APSAV1 = new Task();
      task2APSAV1.setTaskType(taskTypeAPS);
      task2APSAV1.setEvaluation(evaluation2AV1);
      task2APSAV1.setDescription("Este é um exemplo do que poderia ser um requisito para APS da AV1");
      task2APSAV1.setMaxNote(Float.parseFloat("3.0"));
      task2APSAV1.setCompleted(true);
      session.save(task2APSAV1);

      // insere uma tarefa da AV2 do tipo trabalho acadêmico pro usuario 2
      Task task2TAAV2 = new Task();
      task2TAAV2.setTaskType(taskTypeTA);
      task2TAAV2.setEvaluation(evaluation2AV2);
      task2TAAV2.setDescription("Este é um exemplo do que poderia ser um requisito para o trabalho acadêmico da AV2");
      task2TAAV2.setMaxNote(Float.parseFloat("8.0"));
      task2TAAV2.setCompleted(true);
      session.save(task2TAAV2);

      // insere uma tarefa da AV2 do tipo APS pro usuario 2
      Task task2APSAV2 = new Task();
      task2APSAV2.setTaskType(taskTypeAPS);
      task2APSAV2.setEvaluation(evaluation2AV2);
      task2APSAV2.setDescription("Este é um exemplo do que poderia ser um requisito para a APS da AV2");
      task2APSAV2.setMaxNote(Float.parseFloat("2.0"));
      session.save(task2APSAV2);

      // insere uma tarefa da AV3 do tipo trabalho acadêmico pro usuario 2
      Task task2TAAV3 = new Task();
      task2TAAV3.setTaskType(taskTypeTA);
      task2TAAV3.setEvaluation(evaluation2AV3);
      task2TAAV3.setDescription("Este é um exemplo do que poderia ser um requisito para a o trabalho acadêmico da AV3");
      task2TAAV3.setMaxNote(Float.parseFloat("10.0"));
      task2TAAV3.setCompleted(true);
      session.save(task2TAAV3);

      session.getTransaction().commit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
