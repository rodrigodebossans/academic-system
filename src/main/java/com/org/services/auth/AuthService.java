package com.org.services.auth;

import com.org.models.user.User;
import com.org.services.user.UserService;

public class AuthService {
  UserService userService = new UserService();

  public AuthService() {
  }

  public User authenticate(String registration, String password) {
    User user = this.userService.findByRegistration(registration);

    if(user != null && user.getRegistration().equals(registration) && user.getPassword().equals(password))
      return user;

    return null;
  }
}
