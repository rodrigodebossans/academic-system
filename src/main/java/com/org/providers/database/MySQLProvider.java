package com.org.providers.database;

import com.org.providers.properties.ApplicationPropertiesProvider;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLProvider {
  private static Connection instance;
  
  public static Connection getInstance() throws ClassNotFoundException, SQLException {

    ApplicationPropertiesProvider applicationPropertiesProvider = ApplicationPropertiesProvider.getInstance();
    
    final String ip = applicationPropertiesProvider.getProperty("database.ip");
    final String port = applicationPropertiesProvider.getProperty("database.port");
    final String user = applicationPropertiesProvider.getProperty("database.user");
    final String password = applicationPropertiesProvider.getProperty("database.password");

    if (instance == null) {
      Class.forName("com.mysql.cj.jdbc.Driver");
      instance = DriverManager.getConnection("jdbc:mysql://"+ ip +":"+ port +"/", user, password);
    }
    return instance;
  }
}
