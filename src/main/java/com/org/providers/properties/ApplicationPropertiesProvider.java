package com.org.providers.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationPropertiesProvider {

  private static ApplicationPropertiesProvider instance;
  private Properties properties;

  private ApplicationPropertiesProvider() {
    this.properties = new Properties();
    this.loadProperties();
  }

  public synchronized static ApplicationPropertiesProvider getInstance() {
    if (instance == null) {
      instance = new ApplicationPropertiesProvider();
    }
    return instance;
  }

  private void loadProperties() {
    try {
      InputStream is = getClass().getClassLoader().getResourceAsStream("application.properties");
      this.properties.load(is);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getProperty(String key) {
    return properties.getProperty(key);
  }
}
