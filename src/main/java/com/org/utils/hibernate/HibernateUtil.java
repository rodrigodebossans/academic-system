package com.org.utils.hibernate;

import com.org.providers.properties.ApplicationPropertiesProvider;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@SuppressWarnings("deprecation")
public class HibernateUtil {

  private static final SessionFactory sessionFactory;
  private static final ApplicationPropertiesProvider applicationPropertiesProvider = ApplicationPropertiesProvider.getInstance();

  static {
    final String usernameKey = "hibernate.connection.username";
    final String passwordKey = "hibernate.connection.password";
    final String urlKey = "hibernate.connection.url";
    
    final String username = applicationPropertiesProvider.getProperty(usernameKey);
    final String password = applicationPropertiesProvider.getProperty(passwordKey);
    final String url = applicationPropertiesProvider.getProperty(urlKey);

    try {
      Configuration configuration = new Configuration();
      configuration.setProperty(usernameKey, username);
      configuration.setProperty(passwordKey, password);
      configuration.setProperty(urlKey, url);

      sessionFactory = configuration.configure().buildSessionFactory();
    } catch (Throwable ex) {
      System.err.println("Não foi possível criar a Session Factory. "+ ex);
      throw new ExceptionInInitializerError(ex);
    }
  }

  public static SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}
