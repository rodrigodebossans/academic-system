package com.org.models.others;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CURSO")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="id_grau_academico", nullable=false)
    private AcademicDegree academicDegree;
    
    @Column(name = "nome", nullable = false, length = 100)
    private String name;
    
    @Column(name = "carga_horaria", nullable = false)
    private Integer workload;
    
    @Column(name = "cr_minimo", nullable = false)
    private Float cr_minimo;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "TB_CURSO_DISCIPLINA",
            joinColumns = { @JoinColumn(name = "id_curso") },
            inverseJoinColumns = { @JoinColumn(name = "id_disciplina") }
    )
    List<Discipline> disciplineList = new ArrayList<Discipline>();
    
    public Course() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AcademicDegree getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(AcademicDegree academicDegree) {
        this.academicDegree = academicDegree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWorkload() {
        return workload;
    }

    public void setWorkload(Integer workload) {
        this.workload = workload;
    }

    public Float getCr_minimo() {
        return cr_minimo;
    }

    public void setCr_minimo(Float cr_minimo) {
        this.cr_minimo = cr_minimo;
    }

    public List<Discipline> getDisciplineList() {
        return disciplineList;
    }

    public void setDisciplineList(List<Discipline> disciplineList) {
        this.disciplineList = disciplineList;
    }
    
    public void addDiscipline(Discipline discipline) {
        this.disciplineList.add(discipline);
    }
}
