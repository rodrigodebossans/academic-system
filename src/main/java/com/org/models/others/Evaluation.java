package com.org.models.others;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_AVALIACAO")
public class Evaluation {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name="id_tipo_avaliacao", nullable=false)  
    private EvaluationType evaluationType;
    
    @ManyToOne
    @JoinColumn(name="id_aluno_turma", nullable=false)
    private StudentClass studentClass;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inicio", nullable = false)
    private java.util.Date initDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_fim", nullable = false)
    private java.util.Date endDate;
    
    @Column(name = "nota_final", nullable = true)
    private Float finalNote = Float.parseFloat("0.0");

    @Column(name = "nota_maxima", nullable = true)
    private Float maxNote;
    
    @Column(name = "concluida", nullable = false)
    private boolean completed = false;

    public Evaluation() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EvaluationType getEvaluationType() {
        return evaluationType;
    }

    public void setEvaluationType(EvaluationType evaluationType) {
        this.evaluationType = evaluationType;
    }

    public StudentClass getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(StudentClass studentClass) {
        this.studentClass = studentClass;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Float getFinalNote() {
        return finalNote;
    }

    public void setFinalNote(Float finalNote) {
        this.finalNote = finalNote;
    }

    public Float getMaxNote() {
        return maxNote;
    }

    public void setMaxNote(Float maxNote) {
        this.maxNote = maxNote;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "Evaluation{" + "id=" + id + ", evaluationType=" + evaluationType + ", studentClass=" + studentClass + ", initDate=" + initDate + ", endDate=" + endDate + ", finalNote=" + finalNote + ", maxNote=" + maxNote + '}';
    }
}
