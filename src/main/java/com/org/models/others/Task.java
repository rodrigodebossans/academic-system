package com.org.models.others;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_TAREFA")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "id_tipo_tarefa", nullable = false)
    private TaskType taskType;

    @ManyToOne
    @JoinColumn(name = "id_avaliacao", nullable = false)
    private Evaluation evaluation;
    
    @Column(name = "descricao", nullable = true)
    private String description;
    
    @Column(name = "nota", nullable = true)
    private Float note = Float.parseFloat("0.0");
    
    @Column(name = "concluida", nullable = false)
    private boolean completed = false;

    @Column(name = "nota_maxima", nullable = false)
    private Float maxNote;

    public Task() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getNote() {
        return note;
    }

    public void setNote(Float note) {
        this.note = note;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Float getMaxNote() {
        return maxNote;
    }

    public void setMaxNote(Float maxNote) {
        this.maxNote = maxNote;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id + ", taskType=" + taskType + ", evaluation=" + evaluation + ", description=" + description + ", note=" + note + ", completed=" + completed + ", maxNote=" + maxNote + '}';
    }
}
