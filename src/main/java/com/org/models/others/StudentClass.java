package com.org.models.others;

import com.org.models.user.Student;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_ALUNO_TURMA")
public class StudentClass {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "id_aluno")
  private Student student;

  @ManyToOne
  @JoinColumn(name = "id_turma")
  private Class classe;

  public StudentClass() {}

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public Class getClasse() {
    return classe;
  }

  public void setClasse(Class classe) {
    this.classe = classe;
  }

  @Override
  public String toString() {
    return "StudentClass{" + "id=" + id + ", student=" + student + ", classe=" + classe + '}';
  }
}
