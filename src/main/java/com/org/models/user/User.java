package com.org.models.user;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.org.models.role.Role;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "TB_USUARIO")
public class User {

  @Id
  @Column(name = "cpf", nullable = false, unique = true, length = 100)
  private String cpf;
  
  @ManyToOne
  @JoinColumn(name="id_role", nullable=false)
  private Role role;
  
  @Column(name = "matricula", nullable = false)
  private String registration;
  
  @Column(name = "nome", nullable = false)
  private String name;
  
  @Column(name = "sobrenome", nullable = false)
  private String surname;
  
  @Column(name = "senha", nullable = false)
  private String password;
  
  public User() {}

  public User(String registration, String name, String surname, String password, Role role) {
    this.registration = registration;
    this.name = name;
    this.surname = surname;
    this.password = password;
    this.role = role;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
  
  public String getRegistration() {
    return registration;
  }

  public void setRegistration(String registration) {
    this.registration = registration;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getFullName() {
    return this.name + " " + this.surname;
  }
  
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  @Override
  public String toString() {
    return "User{" + "cpf=" + cpf + ", role=" + role + ", registration=" + registration + ", name=" + name + ", surname=" + surname + ", password=" + password + '}';
  }
}