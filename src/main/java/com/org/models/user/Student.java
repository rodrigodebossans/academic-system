package com.org.models.user;

import com.org.models.others.Course;
import com.org.models.others.StudentClass;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Table(name = "TB_ALUNO")
public class Student {
      
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "cpf_pessoa", referencedColumnName = "cpf")
  private User user;
  
  @ManyToOne
  @JoinColumn(name="id_curso", nullable = true)
  private Course course;
  
  @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<StudentClass> studentClassList = new ArrayList<StudentClass>();
  
  public Student() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public List<StudentClass> getStudentClassList() {
    return studentClassList;
  }

  public void setStudentClassList(List<StudentClass> studentClassList) {
    this.studentClassList = studentClassList;
  }

  @Override
  public String toString() {
    return "Student{" + "\n"
            + "\tid=" + id + ", \n"
            + "\tuser={ user.name=" + user.getFullName() + "}, \n"
            + "\tcourse= { course.name=" + course.getName() + "}\n"
            + "\tStudentClass= {" + "\n"
            + "\t}"
            + "\n}";
  }

}