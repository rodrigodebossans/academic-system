<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="intercepters/auth/CheckLoggedUser.jsp"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.org.models.user.User"%>
<%@page import="com.org.models.others.Evaluation"%>
<%@page import="com.org.models.others.StudentClass"%>
<%@page import="com.org.models.others.Task"%>

<jsp:useBean id="studentService" class="com.org.services.user.StudentService" />
<jsp:useBean id="taskService" class="com.org.services.others.TaskService" />
<jsp:useBean id="userService" class="com.org.services.user.UserService" />

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css" />
    <title>Academic System - Home</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="home-professor.html">AC</a>
      <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="home.jsp"
              >Home <span class="sr-only">(current)</span></a
            >
          </li>
          <% User userLogged = (User) session.getAttribute("userLogged"); %>
        </ul>
        <form method="POST" action="logoutController">
          <button
            type="submit"
            id="logout-button"
            class="btn btn-sm btn-outline-danger my-2 my-sm-0"
          >
            Sair
          </button>
      </form>
      </div>
    </nav>

    <main class="container bg-white p-5 mt-5 rounded">
      <section class="row">
        <article class="col-12 d-flex justify-content-between">
          <h1 class="h3">Notas</h1>
        </article>
      </section>

      <section id="grades" class="row mt-4">
        <% 
          List<User> userList = new ArrayList<User>();
          if (userLogged != null && userService.isTeacher(userLogged.getRegistration())) {
              userList = userService.getStudentList();
          } else if (userLogged != null && userService.isStudent(userLogged.getRegistration())) {
              userList.add(userLogged);
          }
          Integer numberOfUsersInTheList = userList.size();
          if (!userList.isEmpty())
            for (User user: userList) {
              StudentClass studentClass = studentService.getStudentClassByRegistration(user.getRegistration());
        %>
              <article class="col-12 <%=numberOfUsersInTheList > 1 ? "col-md-6" : "col-md-12"%>">
                <div class="card">
                  <div class="card-header d-flex justify-content-between">
                    <div>
                      <small class="card-title mb-0 nome-aluno text-center">
                        <b><%=user.getFullName()%></b>
                      </small>
                      <br>
                      <small class="card-title mb-0 nome-aluno text-center">
                        <%=user.getRegistration()%>
                      </small>
                    </div>
                    <% if (userLogged != null && userService.isTeacher(userLogged.getRegistration())) { %>
                      <button
                      class="btn btn-sm btn-secondary align-self-center"
                      onclick="throwNote(<%=user.getRegistration()%>, '<%=user.getFullName()%>')"
                      >
                        Lançar nota
                      </button>
                    <% } %>
                  </div>
                  <div class="card-body">
                    <ul class="list-group mb-3">
                      <%
                        List<Evaluation> evaluationList = studentService.getEvaluationsByRegistration(user.getRegistration());
                        Float average = studentService.getAverageByRating(evaluationList);
                        if (!evaluationList.isEmpty()) 
                          for (Evaluation evaluation : evaluationList) {
                            String evaluationTypeName = evaluation.getEvaluationType().getName();
                            String registrationUser = user.getRegistration();
                      %>
                      <li class="list-group-item">
                          <div class="d-flex justify-content-between align-self-center">
                            <small class="pt-2"><b>Nota <%=evaluationTypeName%>:</b> <%=evaluation.isCompleted() ? evaluation.getFinalNote() : "nota não lançada" %></small>
                            <a class="btn btn-secondary btn-sm" data-toggle="collapse" href="#details<%=evaluationTypeName+registrationUser%>" role="button" aria-expanded="false" aria-controls="details<%=evaluationTypeName+registrationUser%>">
                              Mais detalhes
                            </a>
                          </div>
                          <div class="collapse mt-3" id="details<%=evaluationTypeName+registrationUser%>">
                            <div class="card card-body">
                              <%
                                List<Task> taskList = taskService.getTaskListByEvaluationId(evaluation.getId());
                                if (!taskList.isEmpty()) {
                                  for (Task task : taskList) {
                              %>
                                    <small><b><%=task.getTaskType().getName()%>: </b> </small>
                                    <br>
                                    <div class="card">
                                      <div class="card-body">
                                        <small><b>Descrição: </b> <%=task.getDescription()%> </small>
                                        <br>
                                        <small><b>Valor da tarefa: </b> <%=task.getMaxNote()%> </small>
                                        <br>
                                        <small><b>Nota do aluno: </b> <%=task.getNote()%> </small>
                                        <br>
                                        <small><b>Situação: </b> <%=task.isCompleted() ? "tarefa realizada" : "tarefa não realizada"%> </small>
                                      </div>
                                    </div>
                                    <br>
                              <%  } 
                                } else {
                              %>
                                <small>Nenhuma atividade realizada na <%=evaluationTypeName%></small>
                              <%}%>
                            </div>
                          </div>
                      </li>
                      <%}%>
                    </ul>
                  </div>
                  <div class="card-footer d-flex justify-content-between">
                    <small><b>Média final:</b> <%=average%> </small>
                    <small><b> Situação: </b> <%=studentService.getStudentSituation(average)%></small>
                  </div>
                </div>          
              </article>
        <%  } %>
      </section>
    </main>

    <% if (userLogged != null && userService.isTeacher(userLogged.getRegistration())) { %>
      <div class="modal fade" id="modalThrowNotes" tabindex="-1" role="dialog" aria-labelledby="modalThrowNotes" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalTitle">Cadastrar nota</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form id="noteForm" method="POST" action="noteController">
              <div class="modal-body">
                  <div class="row">
                    <div class="col-6">
                      <select id="evaluationTypes" name="evaluationType" class="form-control form-control-sm" required>
                      </select>
                    </div>
                    <div class="col-6">
                      <input id="note" name="note" type="number" step="0.1" min="0" max="10" class="form-control form-control-sm" placeholder="Nota" required>
                    </div>
                  </div>
              </div>
              <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-sm btn-secondary">Salvar alterações</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <% } %>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js" integrity="sha512-/DXTXr6nQodMUiq+IUJYCt2PPOUjrHJ9wFrqpJ3XkgPNOZVfMok7cRw6CSxyCQxXn6ozlESsSh1/sMCTF1rL/g==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <script src="js/home.js"></script>
  </body>
</html>
