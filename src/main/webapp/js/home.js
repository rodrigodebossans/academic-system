function throwNote(registration, name) {
    const url = window.location.origin + '/academic-system/noteController?registration=' + registration;
    $("#modalTitle").html(`Lançar nota para o aluno ${name}`);
    fetch(url)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
        if (data) {
            $("#evaluationTypes").append(`<option disabled selected>Selecione uma avaliação</option>`);
            Array.from(data).forEach(evaluation => {
                $("#evaluationTypes").append(`<option value=${evaluation.evaluationId}>${evaluation.evaluationType}</option>`);
            });
            $('#modalThrowNotes').modal('show');
        } else {
            alert("erro ao buscar dados no servidor");
        }
    });
}

$('#modalThrowNotes').on('hidden.bs.modal', function (e) {
    $("#evaluationTypes").html(``);
});