<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
      integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
      crossorigin="anonymous"
    />
    <title>Academic System - Login</title>
  </head>
  <body>
    <main class="container">
      <section class="row mt-5">
        <article class="col-12 col-md-4 offset-md-4">
          <form name="loginForm" class="form-signin" method="POST" action="loginController">
            <h1 class="h3 font-weight-normal text-center">Academic System</h1>
            <p class="mb-3 font-weight-normal text-center">Entrar no sistema</p>
            
            <% if (session.getAttribute("authErrorMessage") == "invalidUser") { %>
                <div class="alert alert-danger status-message" role="alert">
                    Usuário ou matrícula inexistente
                </div>
            <% } %>
            
            <label for="email" class="sr-only">Email</label>
            <input
              type="number"
              name="registration"
              class="form-control mb-2"
              placeholder="Matrícula"
              required
              autofocus
            />
            <input
              type="password"
              name="password"
              class="form-control mb-2"
              placeholder="Senha"
              required
            />
            <button
              class="btn btn-md btn-primary btn-block"
              type="submit"
              onclick=""
            >
              Entrar
            </button>
            <p class="text-center mt-3">
              <small class="mt-5 mb-3 text-muted">
                &copy; Academic System - Todos os direitos reservados
              </small>
            </p>
          </form>
        </article>
      </section>
    </main>
  </body>
</html>
